% railcontrol(1) | User Commands
%
% November 2024

# NAME

railcontrol - A software for model railway control

# SYNOPSIS

**railcontrol** [**\-\-config=ConfigFile**] [**-d** | **\-\-daemonize**]
                [**\-\-logfile=LogFile**] [**-h** | **\-\-help**]
                [**-s** | **\-\-silent**]

# DESCRIPTION

This manual page documents briefly the **railcontrol** command.

This manual page was written for the Debian distribution because the
original program does not have a manual page.

**RailControl** is a software for model railway control. The handling is done
with a webbrowser on the device of your choice.

# OPTIONS

The program follows the usual GNU command line syntax, with long options
starting with two dashes ('-'). A summary of options is included below.

**\-\-config=**_ConfigFile_
:   Read config file with file name ConfigFile (default ConfigFile: railcontrol.conf).

**-d**, **\-\-daemonize**
:   Daemonize RailControl. Implies -s.

**\-\-logfile=**_LogFile_
:   Write a logfile to file LogFile (default LogFile: railcontrol.log).

**-h**, **\-\-help**
:   Show summary of options.

**-s**, **\-\-silent**
:   Omit writing to console.

# FILES

railcontrol.conf
:   The configuration file to control the behaviour of railcontrol. A template
is located at /usr/share/doc/railcontrol/railcontrol.conf.dist. Copy that file
to the directory of your choice and edit it to your needs.

# SEE ALSO

For extensive documentation of RailControl go to https://www.railcontrol.org/index.php/en/documentation-en.

# COPYRIGHT

Copyright © 2024 Bruno Kleinert \<fuddl@debian.org\>

This manual page was written for the Debian system (and may be used by
others).

Permission is granted to copy, distribute and/or modify this document under
the terms of the GNU General Public License, Version 2 or (at your option)
any later version published by the Free Software Foundation.

On Debian systems, the complete text of the GNU General Public License
can be found in /usr/share/common-licenses/GPL.

